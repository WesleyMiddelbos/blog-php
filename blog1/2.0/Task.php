<?php
require_once "Db.php";

function showTasks($selectedTask = null){
    $conn = getdb();

    if (is_null($selectedTask)){
        $q_showTask = $conn->query("SELECT * FROM task");
    }else{
        $q_showTask = $conn->query("SELECT * FROM task WHERE " . $selectedTask . " = task");
    }

    $results = [];
    while($row = $q_showTask->fetch()){
        $results[] = $row;
    }
    return $results;
}

function storeTask($title = null, $content = null, $statusid = null){
    $conn = getdb();

    $statusid = is_null($statusid)? "" : $statusid;
    $title = is_null($title)? "" : $title;
    $content = is_null($content)? "": $content;

    $q_storeTask = $conn->prepare("INSERT INTO task (title, content, date, statusid) VALUES (?, ?, ?, ?)");

    if ($q_storeTask->execute([$title, $content, 'CURRENT_DATE', $statusid])){
        return "Er is iets misgegaan met het toevoegen van de task!";
    }
}

function updateTask($id = null, $title = null, $content = null){
    $conn = getdb();

    $id = is_null($id)? "" : $id;
    $title = is_null($title)? "" : $title;
    $content = is_null($content)? "": $content;

    $sql = $conn->prepare("UPDATE task SET title = ?, content = ? WHERE id = ?");
    $sql->execute([$title, $content, $id]);
}

function deleteTask($id = null){
    $conn = getdb();
    $id = is_null($id)? "" : $id;

    $q_delTask = $conn->prepare("DELETE FROM task WHERE id = ?");

    if (!$q_delTask->execute([$id])){
        return "Er is iets misgegaan met het verwijderen van de task!";
    }
}

function changeStatusTask($id = null, $statusid = null){
    $conn = getdb();
    $statusid = is_null($statusid)? 3 : $statusid;
    $id = is_null($id)? "" : $id;

    $sql = $conn->prepare("UPDATE task SET statusid = ? WHERE id = ?");
    $sql->execute([$statusid, $id]);

}
?>