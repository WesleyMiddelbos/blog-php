<?php
// Report all PHP errors (see changelog)
error_reporting(E_ALL);

// Report all PHP errors
error_reporting(-1);
require_once ('./Task.php');
require_once ('./Status.php');
$tasks = showTasks();
$status = showStatus();
?>

    <div style="margin: 10px 10px 10px 10px;">
        <form action="./route.php" method="post">
            <input type="text" name="title" placeholder="title">
            <textarea name="content" id="" cols="30" rows="10"></textarea>
            <select name="status" >
                <?php
                foreach($status as $state){
                    ?>
                    <option value="<?=$state['id']?>"><?=$state['description']?></option>
                    <?php
                }
                ?>
            </select>
            <input type="submit" name="addTask" value="Toevoegen">
        </form>
    </div>
<?php
if($tasks != null) {
    foreach ($tasks as $task) {
        ?>
        <div style="margin: 10px 10px 10px 10px; border: 1px solid #000;">
            <form action="./route.php" method="post">
                <input type="hidden" name="id" value="<?=$task['id'];?>">
                <input type="text" name="title" value="<?=$task['title'];?>">
                <textarea name="content" id="" cols="30" rows="10"><?=$task['content'];?></textarea>
                <select name="status">
                    <?php
                    foreach($status as $state){
                        ?>
                        <option <?php echo ($state['id'] == $task['statusid'])? "selected" : ""; ?> value="<?=$state['id']?>"><?=$state['description']?></option>
                        <?php
                    }
                    ?>
                </select>
                <input type="submit" name="updateTask" value="Update">
                <input type="submit" name="deleteTask" value="Verwijder">
            </form>
        </div>
        <?php
    }
}
?>