<?php
require_once ('./Task.php');

if(isset($_POST['addTask'])){
    $title = (isset($_POST['title']))? $_POST['title']: null;
    $content = (isset($_POST['content']))? $_POST['content']: null;
    $statusid = (isset($_POST['status']))? $_POST['status']: null;

    storeTask($title, $content, $statusid);

    goBack();
}

if(isset($_POST['updateTask'])){
    updateTask($_POST['id'], $_POST['title'], $_POST['content']);
    changeStatusTask($_POST['id'], $_POST['status']);

    goBack();
}

if(isset($_POST['deleteTask'])){
    deleteTask($_POST['id']);
    goBack();
}

function goBack(){
    header('Location: ./index.php');
}