<?php
require_once "Db.php";

function showTasks($selectedTask = null){
    $conn = getdb();

    if (is_null($selectedTask)){
        $q_showTask = "SELECT * FROM task";
    }else{
        $q_showTask = "SELECT * FROM task WHERE " . $selectedTask . " = task";
    }

    if ($res_showTask = mysqli_query($conn, $q_showTask)){
        $resultsarray = [];
        while($row = mysqli_fetch_assoc($res_showTask)){
            $resultsarray[] = $row;
        }
        return $resultsarray;
    }else{
        return "Er is iets misgegaan met het ophalen van de task(s)!";
    }
}

function storeTask($title = null, $content = null, $statusid = null){
    $conn = getdb();

    $statusid = is_null($statusid)? "" : $statusid;
    $title = is_null($title)? "" : $title;
    $content = is_null($content)? "": $content;

    $q_storeTask = "INSERT INTO task (title, content, date, statusid) VALUES ('".$title."', '".$content."', CURRENT_DATE, '".$statusid."')";

    if (!mysqli_query($conn, $q_storeTask)){
        return "Er is iets misgegaan met het toevoegen van de task!";
    }
}

function updateTask($id, $title = null, $content = null){
    $conn = getdb();

    $id = $_POST['id'];
    $title = is_null($title)? "" : $title;
    $content = is_null($content)? "": $content;

    $sql = "UPDATE task SET title = '".$title."', content = '".$content."' WHERE id = '".$id."'";

    mysqli_query($conn, $sql);

}

function deleteTask($id){
    $conn = getdb();

    $q_delTask = "DELETE FROM task WHERE id = " . $id;

    if (!mysqli_query($conn, $q_delTask)){
        return "Er is iets misgegaan met het verwijderen van de task!";
    }
}

function changeStatusTask($id, $statusid = null){
    $conn = getdb();
    $statusid = is_null($statusid)? 3 : $statusid;
    $id = $_POST['id'];
    $sql = "UPDATE task SET statusid = '".$statusid."' WHERE id = ".$id;

    mysqli_query($conn, $sql);
}
?>