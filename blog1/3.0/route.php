<?php
require_once ('./Task.php');
$route = new Route();
class Route{

    public function __construct(){
        if(isset($_POST['addTask'])){
            $tasks = new Task();
            $title = (isset($_POST['title']))? $_POST['title']: null;
            $content = (isset($_POST['content']))? $_POST['content']: null;
            $statusid = (isset($_POST['status']))? $_POST['status']: null;

            $tasks->storeTask($title, $content, $statusid);

            $this->goBack();
        }

        if(isset($_POST['updateTask'])){
            $tasks = new Task();
            $tasks->updateTask($_POST['id'], $_POST['title'], $_POST['content']);
            $tasks->changeStatusTask($_POST['id'], $_POST['status']);

            $this->goBack();
        }

        if(isset($_POST['deleteTask'])){
            $tasks = new Task();
            $tasks->deleteTask($_POST['id']);
            $this->goBack();
        }
    }

    public function goBack(){
        header('Location: ./index.php');
    }
}
