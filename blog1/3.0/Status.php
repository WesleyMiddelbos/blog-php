<?php
require_once('./Db.php');

class Status extends Db{
    private $conn;

    public function __construct(){
        $this->conn = $this->getdb();
    }

    public function showStatus(){
        $sql = $this->conn->query("SELECT * FROM status");

        $results = [];
        while($fetch = $sql->fetch()){
            $results[] = $fetch;
        }
        return $results;
    }
}

?>