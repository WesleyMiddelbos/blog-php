<?php
require_once "Db.php";

final class Task extends Db {
    private $conn;

    public function __construct(){
        $this->conn = $this->getdb();
    }

    public function showTasks($selectedTask = null){

        if (is_null($selectedTask)){
            $q_showTask = $this->conn->query("SELECT * FROM task");
        }else{
            $q_showTask = $this->conn->query("SELECT * FROM task WHERE " . $selectedTask . " = task");
        }

        $results = [];
        while($row = $q_showTask->fetch()){
            $results[] = $row;
        }
        return $results;
    }

    public function storeTask($title = null, $content = null, $statusid = null){
        $statusid = is_null($statusid)? "" : $statusid;
        $title = is_null($title)? "" : $title;
        $content = is_null($content)? "": $content;

        $q_storeTask = $this->conn->prepare("INSERT INTO task (title, content, date, statusid) VALUES (?, ?, ?, ?)");

        if ($q_storeTask->execute([$title, $content, 'CURRENT_DATE', $statusid])){
            return "Er is iets misgegaan met het toevoegen van de task!";
        }
    }

    public function updateTask($id = null, $title = null, $content = null){
        $id = is_null($id)? "" : $id;
        $title = is_null($title)? "" : $title;
        $content = is_null($content)? "": $content;

        $sql = $this->conn->prepare("UPDATE task SET title = ?, content = ? WHERE id = ?");
        $sql->execute([$title, $content, $id]);
    }

    public function deleteTask($id = null){
        $id = is_null($id)? "" : $id;

        $q_delTask = $this->conn->prepare("DELETE FROM task WHERE id = ?");

        if (!$q_delTask->execute([$id])){
            return "Er is iets misgegaan met het verwijderen van de task!";
        }
    }

    public function changeStatusTask($id = null, $statusid = null){
        $statusid = is_null($statusid)? 3 : $statusid;
        $id = is_null($id)? "" : $id;

        $sql = $this->conn->prepare("UPDATE task SET statusid = ? WHERE id = ?");
        $sql->execute([$statusid, $id]);

    }
}
?>