<?php

namespace App\Http\Handlers;


use App\Task;
use Carbon\Carbon;

class TasksHandler
{
    //
    public function store($request){
        $task = new Task();
        $task->fill($request);
        $mytime = Carbon::now();
        $task->date = $mytime->toDateTimeString();
        $task->save();
    }

    public function delete($id){
        $task = Task::where('id',$id);
        $task->delete();
    }

    public function update($request, $id){
        Task::where('id',$id)->update(['title'=>$request['title'],'content'=>$request['content'],'statusid'=>$request['statusid']]);
    }
}
