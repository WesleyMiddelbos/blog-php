<?php

namespace App\Http\Controllers;

use App\Http\Handlers\TasksHandler;
use App\Status;
use App\Task;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    //
    public function index(){
        $tasks = Task::with('Status')->get();
        $status = Status::get();

        return view('home', ['tasks' => $tasks, 'status'=>$status]);
    }
}
