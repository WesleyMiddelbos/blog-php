<?php

namespace App\Http\Controllers;

use App\Http\Handlers\TasksHandler;
use Illuminate\Http\Request;

class TaskController extends Controller
{
    //
    public function store(Request $request){
        if($request->input('title') == "" || $request->input('content') == ""){
            return redirect()->back();
        }
        $handler = new TasksHandler();
        $handler->store($request->all());

        return redirect()->back();
    }

    public function update(Request $request, $id){
        $handler = new TasksHandler();
        if($request->has('deleteTask')){
            $handler->delete($id);
        }elseif($request->has('updateTask')){
            $handler->update($request->all(), $id);
        }
        return redirect()->back();
    }
}
