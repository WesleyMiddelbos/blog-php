<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    //
    protected $fillable = ['title','content','statusid'];
    public $timestamps = false;

    public function status(){
        return $this->hasOne('App\Status','id','statusid');
    }
}
