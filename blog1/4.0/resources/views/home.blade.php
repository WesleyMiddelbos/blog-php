<div style="margin: 10px 10px 10px 10px;">
    <form action="./task" method="post">
        {{ csrf_field()}}
        <input type="text" name="title" placeholder="title">
        <textarea name="content" id="" cols="30" rows="10"></textarea>
        <select name="statusid" >
            @foreach($status as $state)
                <option value="{{ $state['id'] }}">{{ $state['description'] }}</option>
            @endforeach
        </select>
        <input type="submit" name="addTask" value="Toevoegen">
    </form>
</div>

@foreach($tasks as $task)
<div style="margin: 10px 10px 10px 10px; border: 1px solid #000;">
    <form action="./task/{{ $task['id'] }}" method="post">
        {{ csrf_field()}}
        <input type="text" name="title" value="{{ $task['title'] }}">
        <textarea name="content" id="" cols="30" rows="10">{{ $task['content'] }}</textarea>
        <select name="statusid">
            @foreach($status as $state)
                <option @if($state['id'] == $task['statusid']) {{ "selected" }} @endif value="{{ $state['id'] }}">{{ $state['description'] }}</option>
            @endforeach
        </select>
        <input type="submit" name="updateTask" value="Update">
        <input type="submit" name="deleteTask" value="Verwijder">
    </form>
</div>
@endforeach