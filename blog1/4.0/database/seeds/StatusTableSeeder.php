<?php

use Illuminate\Database\Seeder;

class StatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('statuses')->insert([
            ['id'=>1,'description'=>'open'],
            ['id'=>2,'description'=>'afgerond'],
            ['id'=>3,'description'=>'uitgesteld']
        ]);
    }
}
